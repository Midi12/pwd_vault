package db;

import java.sql.*;

public class DBWrapper
{
	Connection mConn;
	boolean mConnected;

	public DBWrapper()
	{
		mConn = null;
		mConnected = false;

		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			mConn = DriverManager.getConnection(DBStaticData.DB, DBStaticData.USER, DBStaticData.PWD);
			mConnected = true;
		}
		catch (ClassNotFoundException e)
		{
			System.out.println("DBWrapper:ClassNotFoundException : " + e.getMessage());
			mConn = null;
			mConnected = false;
		}
		catch (SQLException e)
		{
			System.out.println("DBWrapper:SQLException : " + e.getMessage() + " (" + e.getErrorCode() + ")");
			mConn = null;
			mConnected = false;
		}
	}

	public boolean isConnected()
	{
		return mConnected;
	}

}
