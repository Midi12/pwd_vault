package gui;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JMenuBar;
import javax.swing.JTree;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JOptionPane;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeSelectionModel;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.KeyEvent;
import java.awt.Dimension;
import java.awt.Color;

import core.listeners.*;
import db.*;

public class MainForm extends JFrame
{
	private Container mContainer;
	private BorderLayout mBorderLayout;

	private JMenuBar mMenuBar;
	private JMenu mMenuFile;
	private JMenuItem mMIFileNewDb;
	private JMenuItem mMIFileNewFld;
	private JMenuItem mMIFileNewEnt;
	private JMenuItem mMIFileOpenDb;
	private JMenuItem mMIFileNewSaveDb;
	private JMenuItem mMIFileNewSaveDbAs;
	private JMenuItem mMIFileClose;
	private JMenu mMenuEdit;
	private JMenuItem mMIEditEditDb;
	private JMenuItem mMIEditEditFld;
	private JMenuItem mMIEditEditEntry;
	private JMenuItem mMIEditDeleteDb;
	private JMenuItem mMIEditDeleteFolder;
	private JMenuItem mMIEditDeleteEntry;
	private JMenu mMenuSettings;
	private JMenuItem mMISettingsConfigure;
	private JMenu mMenuHelp;
	private JMenuItem mMIShowHelp;

	private JScrollPane mScrollPane;
	private JTree mTreeView;
	private JPanel mStatusBar;

	private DBWrapper mDbWrapper;

	public MainForm()
	{
		// call parent class
		super("pwd_vault");

		// setup container & layout
		mBorderLayout = new BorderLayout();
		mContainer = this.getContentPane();
		mContainer.setLayout(mBorderLayout);

		// setup menu
		mMenuBar = new JMenuBar();
		mMenuBar.setSize(this.getWidth(), 50);
		this.setJMenuBar(mMenuBar);

		mMenuFile = new JMenu();
		mMenuFile.setText("File");
		mMenuFile.setMnemonic(KeyEvent.VK_F);

		mMIFileNewDb = new JMenuItem();
		mMIFileNewDb.setText("New database");
		mMIFileNewDb.addActionListener(new ActionNewDatabase());
		mMenuFile.add(mMIFileNewDb);

		mMIFileNewFld = new JMenuItem();
		mMIFileNewFld.setText("New folder");
		mMIFileNewFld.addActionListener(new ActionNewFolder());
		mMenuFile.add(mMIFileNewFld);

		mMIFileNewEnt = new JMenuItem();
		mMIFileNewEnt.setText("New Entry");
		mMIFileNewEnt.addActionListener(new ActionNewEntry());
		mMenuFile.add(mMIFileNewEnt);

		mMIFileOpenDb = new JMenuItem();
		mMIFileOpenDb.setText("Open database");
		mMIFileOpenDb.addActionListener(new ActionOpenDatabase());
		mMenuFile.add(mMIFileOpenDb);

		mMIFileNewSaveDb = new JMenuItem();
		mMIFileNewSaveDb.setText("Save database");
		mMIFileNewSaveDb.addActionListener(new ActionSaveDatabase());
		mMenuFile.add(mMIFileNewSaveDb);

		mMIFileNewSaveDbAs = new JMenuItem();
		mMIFileNewSaveDbAs.setText("Save database as ...");
		mMIFileNewSaveDbAs.addActionListener(new ActionSaveDatabaseAs());
		mMenuFile.add(mMIFileNewSaveDbAs);

		mMIFileClose = new JMenuItem();
		mMIFileClose.setText("Close");
		mMIFileClose.addActionListener(new ActionCloseApplication());
		mMenuFile.add(mMIFileClose);

		mMenuBar.add(mMenuFile);

		mMenuEdit = new JMenu();
		mMenuEdit.setText("Edit");
		mMenuEdit.setMnemonic(KeyEvent.VK_E);

		mMIEditEditDb = new JMenuItem();
		mMIEditEditDb.setText("Edit database");
		mMIEditEditDb.addActionListener(new ActionEditDatabase());
		mMenuEdit.add(mMIEditEditDb);

		mMIEditEditFld = new JMenuItem();
		mMIEditEditFld.setText("Edit folder");
		mMIEditEditFld.addActionListener(new ActionEditFolder());
		mMenuEdit.add(mMIEditEditFld);

		mMIEditEditEntry = new JMenuItem();
		mMIEditEditEntry.setText("Edit entry");
		mMIEditEditEntry.addActionListener(new ActionEditEntry());
		mMenuEdit.add(mMIEditEditEntry);

		mMIEditDeleteDb = new JMenuItem();
		mMIEditDeleteDb.setText("Delete database");
		mMIEditDeleteDb.addActionListener(new ActionDeleteDatabase());
		mMenuEdit.add(mMIEditDeleteDb);

		mMIEditDeleteFolder = new JMenuItem();
		mMIEditDeleteFolder.setText("Delete folder");
		mMIEditDeleteFolder.addActionListener(new ActionDeleteFolder());
		mMenuEdit.add(mMIEditDeleteFolder);

		mMIEditDeleteEntry = new JMenuItem();
		mMIEditDeleteEntry.setText("Delete entry");
		mMIEditDeleteEntry.addActionListener(new ActionDeleteEntry());
		mMenuEdit.add(mMIEditDeleteEntry);

		mMenuBar.add(mMenuEdit);

		mMenuSettings = new JMenu();
		mMenuSettings.setText("Settings");
		mMenuSettings.setMnemonic(KeyEvent.VK_V);

		mMISettingsConfigure = new JMenuItem();
		mMISettingsConfigure.setText("Configure settings");
		mMISettingsConfigure.addActionListener(new ActionConfigureSettings());
		mMenuSettings.add(mMISettingsConfigure);

		mMenuBar.add(mMenuSettings);

		mMenuHelp = new JMenu();
		mMenuHelp.setText("Help");
		mMenuHelp.setMnemonic(KeyEvent.VK_H);

		mMIShowHelp = new JMenuItem();
		mMIShowHelp.setText("Show help");
		mMIShowHelp.addActionListener(new ActionShowHelp());
		mMenuHelp.add(mMIShowHelp);

		mMenuBar.add(mMenuHelp);

		// setup treeview
		// create top node
		DefaultMutableTreeNode topNode = new DefaultMutableTreeNode("/");

		//Create the tree
		mTreeView = new JTree(topNode);
		mTreeView.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);

		Dimension minDim = new Dimension(300, 300);
		mTreeView.setMinimumSize(minDim);

		mScrollPane = new JScrollPane(mTreeView);
		mScrollPane.setVisible(true);
		mScrollPane.setMinimumSize(minDim);
		mScrollPane.setBackground(Color.red);

		// setup layout & menubar
		this.setJMenuBar(mMenuBar);
		mContainer.add(mScrollPane, BorderLayout.NORTH);

		// setup last things
		this.setSize(680, 480);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);

		// setup db
		mDbWrapper = new DBWrapper();

		if (!mDbWrapper.isConnected())
		{
			System.out.println("Cannot connect to database !");
			JOptionPane.showMessageDialog(null, "Cannot connect to database !");
		}
	}
}
