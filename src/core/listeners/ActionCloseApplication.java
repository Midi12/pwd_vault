package core.listeners;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ActionCloseApplication implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		// ask user to save db before closing
			//perform save of db if user requested it
		System.exit(0);
	}
}
