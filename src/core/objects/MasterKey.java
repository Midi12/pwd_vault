/*
+-----------------+-------------+------+-----+---------+-------+
| Field           | Type        | Null | Key | Default | Extra |
+-----------------+-------------+------+-----+---------+-------+
| master_key_hash | varchar(64) | NO   | PRI |         |       |
+-----------------+-------------+------+-----+---------+-------+
*/

package core.objects;

import java.sql.Connection;

public class MasterKey
{
	private static final MasterKey mInstance = new MasterKey();
	private String mKey;
	private Connection mConn;

	private MasterKey()
	{
	}

	public static getInstance()
	{
		return mInstance;
	}

	public void setConn(Connection conn)
	{
		mConn = conn;
	}

	public String setKey(String key)
	{
		//ensure key is valid via hash comparison + throws
		mKey = key;
	}

	public String getKey()
	{
		return mKey;
	}
}
