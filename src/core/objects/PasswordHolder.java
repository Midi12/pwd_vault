/*
+---------------+--------------+------+-----+---------+-------+
| Field         | Type         | Null | Key | Default | Extra |
+---------------+--------------+------+-----+---------+-------+
| pvph_id       | int(11)      | NO   | PRI | 0       |       |
| pvph_name     | varchar(255) | YES  |     | NULL    |       |
| pvph_username | varchar(255) | YES  |     | NULL    |       |
| pvph_password | varchar(255) | YES  |     | NULL    |       |
| pvph_hint     | varchar(42)  | YES  |     | NULL    |       |
| pvf_id        | int(11)      | YES  | MUL | NULL    |       |
+---------------+--------------+------+-----+---------+-------+
*/

package core.objects;

import java.sql.Connection;

public class PasswordHolder
{
	private Connection mConn;

	private int mId;
	private String mName;
	private String mUserName;
	private String mPassword;
	private String mHint;

	public PasswordHolder(Connection connection, String name, String username, String password)
	{
		mConn = connection;
		mId = /*get last id*/;
		mName = name;
		mUserName = username;
		mPassword = password;
		mHint = /*gen random hint*/;
	}

	public int getId()
	{
		return mId;
	}

	public String getName()
	{
		return mName;
	}

	public String getUserName()
	{
		return mUserName;
	}

	public String getPassword()
	{
		return mPassword;
	}

	public String getHint()
	{
		return mHint;
	}

}
