CREATE DATABASE IF NOT EXISTS `VAULTDB` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `VAULTDB`;

CREATE TABLE `PV_PASSWORD_HOLDER` (
  `pvph_id` INT,
  `pvph_name` VARCHAR(255),
  `pvph_username` VARCHAR(255),
  `pvph_password` VARCHAR(255),
  `pvph_hint` VARCHAR(42),
  `pvf_id` INT,
  PRIMARY KEY (`pvph_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `PV_FOLDER` (
  `pvf_id` INT,
  `pvf_name` VARCHAR(225),
  PRIMARY KEY (`pvf_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `PV_MASTER_KEY` (
  `master_key_hash` VARCHAR(64),
  PRIMARY KEY (`master_key_hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `PV_PASSWORD_HOLDER` ADD FOREIGN KEY (`pvf_id`) REFERENCES `PV_FOLDER` (`pvf_id`);
